#!/usr/bin/env ruby

puts 94 * 94
('!'..'R').each do |c1|
  ('!'..'~').each do |c2|
    print c2 * 4, c1 * 3, "\n"
  end
end
('S'..'~').each do |c1|
  ('!'..'~').each do |c2|
    print c1 * 3, c2 * 4, "\n"
  end
end
