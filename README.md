# Foundations of Concurrent and Distributed Systems Lab: Summer semester 2015 #

This repository contains 5 programming tasks, with their descriptions, sequential C sources, and test inputs. The tasks are taken from the 7th Marathon of Parallel Programming WSCAD-SSC/SBAC-PAD-2012.

# Contributors #

Dmitrii Kuvaiskii <dmitrii.kuvaiskii@tu-dresden.de>
